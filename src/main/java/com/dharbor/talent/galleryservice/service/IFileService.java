package com.dharbor.talent.galleryservice.service;

import com.dharbor.talent.galleryservice.domain.File;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * @author Juan Jose Miranda
 */
public interface IFileService {
    File save(MultipartFile file);

    File findById(String fileId);

    List<File> findByParam(String param);

    List<File> findByIds(List<String> ids);
}
