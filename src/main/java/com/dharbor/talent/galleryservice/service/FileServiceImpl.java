package com.dharbor.talent.galleryservice.service;

import com.dharbor.talent.galleryservice.domain.File;
import com.dharbor.talent.galleryservice.repository.FileRepository;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

/**
 * @author Juan Jose Miranda
 */
@Service
public class FileServiceImpl implements IFileService {

    private FileRepository repository;

    @Autowired
    private IFileDataService fileDataService;

    public FileServiceImpl(FileRepository repository) {
        this.repository = repository;
    }

    @Override
    public File save(MultipartFile file) {
        File instance = new File();

        instance.setName(file.getOriginalFilename());
        instance.setMimeType(file.getContentType());
        instance.setSize(file.getSize());
        repository.save(instance);

        try {
            fileDataService.save(instance, file.getBytes());
        } catch (IOException e) {
            throw new RuntimeException("It's not possible save a file data");
        }
        return instance;
    }

    @Override
    public File findById(String fileId) {
        return repository.findById(fileId)
                .orElseThrow(() -> new RuntimeException("Unable to locate a file for fileId " + fileId));
    }

    @Override
    public List<File> findByParam(String param) {
        return repository.findByExtension(param);
    }

    @Override
    public List<File> findByIds(List<String> ids) {
        return repository.findByIdIn(ids);
    }
}
