package com.dharbor.talent.galleryservice.config;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author Juan Jose Miranda
 */
@Configuration
public class OpenApiConfig {
    @Bean
    public OpenAPI openAPI() {
        return new OpenAPI()
                .info(apiInfo());
    }

    private Info apiInfo() {
        return new Info()
                .title("GALLERY SERVICE")
                .description("Documentation of gallery service")
                .version("0.0.1")
                .license(new License()
                        .name("Apache 2")
                        .url("https://www.apache.org/licenses/LICENSE-2.0.html")
                )
                .contact(new Contact()
                        .name("Juan Jose Miranda")
                        .email("juan.miranda@dharbor.com")
                        .url("www.digitalharbor.com")
                );
    }
}
