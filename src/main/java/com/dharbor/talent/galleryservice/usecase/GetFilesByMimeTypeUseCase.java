package com.dharbor.talent.galleryservice.usecase;

import com.dharbor.talent.galleryservice.domain.File;
import com.dharbor.talent.galleryservice.service.IFileService;
import lombok.extern.flogger.Flogger;
import lombok.extern.log4j.Log4j2;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.logging.Logger;

/**
 * @author Juan Jose Miranda
 */
@Service
//@Slf4j
public class GetFilesByMimeTypeUseCase {

    @Autowired
    private IFileService fileService;

//    Logger log = Logger.getLogger(GetFilesByMimeTypeUseCase.class.getName());
//    Logger log = LoggerFactory.getLogger(GetFilesByMimeTypeUseCase.class);

    public List<File> execute(String extension) {

        return fileService.findByParam(extension);
    }
}
