package com.dharbor.talent.galleryservice.usecase;

import com.dharbor.talent.galleryservice.domain.File;
import com.dharbor.talent.galleryservice.domain.FileData;
import com.dharbor.talent.galleryservice.domain.dto.FileDownloadResponse;
import com.dharbor.talent.galleryservice.service.IFileDataService;
import com.dharbor.talent.galleryservice.service.IFileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Juan Jose Miranda
 */
@Service
public class DownloadFileUseCase {

    @Autowired
    private IFileService fileService;

    @Autowired
    private IFileDataService fileDataService;

    public FileDownloadResponse execute(String fileId) {
        File file = fileService.findById(fileId);
        FileData fileData = fileDataService.findByFileId(fileId);

        return buildFileDownloadResponse(file, fileData);
    }

    private FileDownloadResponse buildFileDownloadResponse(File file, FileData fileData) {
        FileDownloadResponse instance = new FileDownloadResponse();

        instance.setName(file.getName());
        instance.setMimeType(file.getMimeType());
        instance.setSize(file.getSize());
        instance.setBytes(fileData.getValue());

        return instance;
    }
}
