package com.dharbor.talent.galleryservice.usecase;

import com.dharbor.talent.galleryservice.domain.File;
import com.dharbor.talent.galleryservice.domain.dto.FileListByIdsInput;
import com.dharbor.talent.galleryservice.service.IFileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Juan Jose Miranda
 */
@Service
public class GetFilesByIdsUseCase {

    @Autowired
    private IFileService fileService;

    public List<File> execute(FileListByIdsInput input) {
        return fileService.findByIds(input.getFileIds());
    }
}
