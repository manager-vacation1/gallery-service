package com.dharbor.talent.galleryservice.domain.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Juan Jose Miranda
 */
@Getter
@Setter
public class FileDownloadResponse {

    private String mimeType;

    private String name;

    private Long size;

    private byte[] bytes;
}
