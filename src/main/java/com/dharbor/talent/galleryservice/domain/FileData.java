package com.dharbor.talent.galleryservice.domain;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author Juan Jose Miranda
 */
@Setter
@Getter
@Document(collection = "filedata")
public class FileData {
    @Id
    private String id;

    private byte[] value;

    @DBRef
    private File file;
}
