package com.dharbor.talent.galleryservice.domain.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @author Juan Jose Miranda
 */
@Setter
@Getter
public class FileListByIdsInput {

    private List<String> fileIds;
}
