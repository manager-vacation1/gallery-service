package com.dharbor.talent.galleryservice.controller;

import com.dharbor.talent.galleryservice.domain.File;
import com.dharbor.talent.galleryservice.domain.dto.FileDownloadResponse;
import com.dharbor.talent.galleryservice.domain.dto.FileListByIdsInput;
import com.dharbor.talent.galleryservice.usecase.CreateFileUseCase;
import com.dharbor.talent.galleryservice.usecase.DownloadFileUseCase;
import com.dharbor.talent.galleryservice.usecase.GetFilesByIdsUseCase;
import com.dharbor.talent.galleryservice.usecase.GetFilesByMimeTypeUseCase;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.List;

/**
 * @author Juan Jose Miranda
 */
@Tag(
        name = "Gallery",
        description = "Operations over files"
)
@RestController
@RequestMapping("/gallery")
public class FileController {

    @Autowired
    private CreateFileUseCase createFileUseCase;

    @Autowired
    private DownloadFileUseCase downloadFileUseCase;

    @Autowired
    private GetFilesByMimeTypeUseCase getFilesByMimeTypeUseCase;

    @Autowired
    private GetFilesByIdsUseCase getFilesByIdsUseCase;

    @Operation(
            summary = "Upload a file"
    )
    @PostMapping(consumes = "multipart/form-data")
    public File uploadFile(@RequestPart MultipartFile file) {
        return createFileUseCase.execute(file);
    }

    @Operation(
            summary = "Download a file"
    )
    @GetMapping(value = "{fileId}/download")
    public ResponseEntity<InputStreamResource> downloadFile(@PathVariable("fileId") String fileId) {
        FileDownloadResponse response = downloadFileUseCase.execute(fileId);
        InputStream content = new ByteArrayInputStream(response.getBytes(), 0, response.getBytes().length);

        return ResponseEntity
                .ok()
                .header(HttpHeaders.CONTENT_DISPOSITION)
                .contentType(MediaType.parseMediaType(response.getMimeType()))
                .body(new InputStreamResource(content));
    }

    @Operation(
            summary = "Get files by extension"
    )
    @GetMapping()
    public List<File> getFilesByMimeType(@RequestParam("extension") String extension) {
        return getFilesByMimeTypeUseCase.execute(extension);
    }

    @Operation(
            summary = "List files by ids"
    )
    @PostMapping(value = "byFileIds")
    public List<File> getFilesByIds(@RequestBody FileListByIdsInput input){
        return getFilesByIdsUseCase.execute(input);
    }
}
