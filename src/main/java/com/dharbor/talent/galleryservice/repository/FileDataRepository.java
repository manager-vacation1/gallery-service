package com.dharbor.talent.galleryservice.repository;

import com.dharbor.talent.galleryservice.domain.FileData;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * @author Juan Jose Miranda
 */
@Repository
public interface FileDataRepository extends MongoRepository<FileData, String> {
    Optional<FileData> findByFileId(String fileId);
}
