package com.dharbor.talent.galleryservice.repository;

import com.dharbor.talent.galleryservice.domain.File;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * @author Juan Jose Miranda
 */
@Repository
public interface FileRepository extends MongoRepository<File, String> {

    @Query(value = "{'name': {$regex: ?0} }")
    List<File> findByExtension(@Param("name") String extension);

    List<File> findByIdIn(List<String> ids);
}
